module.exports = {
    devServer: {
      proxy: {
        '^/api_flask': {
          target: 'http://api_flask:5000',
          ws: true,
          changeOrigin: true,
        },
      },
    },
  }