import HomePage from "../components/HomePage.vue"
import ServicePage from "../components/ServicePage.vue"
import TemoignagePage from "../components/TemoignagePage.vue"
import EquipePage from "../components/EquipePage.vue"
import ContactPage from "../components/ContactPage.vue"
import FormTemoignage from "../components/FormTemoignage.vue"
import ReserverPage from "../components/ReserverPage.vue"

import { createRouter, createWebHistory } from "vue-router"

const routes = [
    {
        path : "/",
        name: "home-page",
        component : HomePage
    },
    {
        path : "/services",
        name: "nos-services",
        component: ServicePage
    },
    {
        path : "/reserver",
        name: "reserver",
        component: ReserverPage
    },
    {
        path: "/temoignage",
        name: "temoignage",
        component: TemoignagePage
    },
    {
        path: "/equipe",
        name: "equipe",
        component: EquipePage
    },
    {
        path: "/contact",
        name: "contact",
        component: ContactPage
    },
    {
        path: "/formTemoignage",
        name: "form-temoignage",
        component: FormTemoignage
    }

]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router
